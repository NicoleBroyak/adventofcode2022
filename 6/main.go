package main

import (
	"log"
	"os"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

type stacksMap [][]byte
type instructionMaps []map[string]int

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	log.Println(findDistinctCharactersPosition(file, 4))

	dur := timer.Stop()
	log.Println(dur)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	log.Println(findDistinctCharactersPosition(file, 14))

	dur := timer.Stop()
	log.Println(dur)
}

func findDistinctCharactersPosition(input []byte, distinctCharactersLen int) int {
	for i := 0; i < len(input)-distinctCharactersLen; i++ {
		characters := input[i : i+distinctCharactersLen]
		usedCharacters := make(map[byte]bool)
		for _, character := range characters {
			usedCharacters[character] = true
		}
		if len(usedCharacters) == distinctCharactersLen {
			return i + distinctCharactersLen
		}
	}
	return 0
}
