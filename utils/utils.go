package utils

import "time"

// simple timing utility
type timer struct {
	timeStart  time.Time
	checkpoint time.Time
}

func StartTimer() *timer {
	return &timer{timeStart: time.Now().UTC(), checkpoint: time.Now().UTC()}
}

func (t *timer) Checkpoint() time.Duration {
	checkpoint := time.Since(t.checkpoint)
	t.checkpoint = time.Now().UTC()
	return checkpoint
}
func (t *timer) Stop() time.Duration {
	dur := time.Since(t.timeStart)
	t = &timer{}
	return dur
}
