package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	treesMap := parseInputIntoTreesMap(scanner)
	count := countVisibleTrees(treesMap)
	log.Println(count)

	dur := timer.Stop()
	log.Println(dur)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	treesMap := parseInputIntoTreesMap(scanner)
	highestScenicScore := getHighestScenicScore(treesMap)
	log.Println(highestScenicScore)

	dur := timer.Stop()
	log.Println(dur)
}

func parseInputIntoTreesMap(scanner *bufio.Scanner) [][]byte {
	var treesMap [][]byte

	for i := 0; scanner.Scan(); {
		row := scanner.Text()
		treesMap = append(treesMap, []byte(row))
		i++
	}

	return treesMap
}

func countVisibleTrees(treesMap [][]byte) int {
	var mapLen = len(treesMap)

	invisibleTrees := make(map[string]bool)

	for y, row := range treesMap {
		for x, height := range row {
			var (
				invisibleFromAbove bool
				invisibleFromBelow bool
				invisibleFromLeft  bool
				invisibleFromRight bool
			)
			treeCoordinate := fmt.Sprintf("%v-%v", y, x)

			// outer trees
			if y == 0 || y == mapLen-1 || x == 0 || x == mapLen-1 {
				continue
			}

			// visible from above
			for i := 0; i < y; i++ {
				if treesMap[i][x] >= height {
					invisibleFromAbove = true
					break
				}
			}

			// visible from below
			for i := mapLen - 1; i > y; i-- {
				if treesMap[i][x] >= height {
					invisibleFromBelow = true
					break
				}
			}

			// visible from the left
			for i := 0; i < x; i++ {
				if row[i] >= height {
					invisibleFromLeft = true
					break
				}
			}

			// visible from the right
			for i := mapLen - 1; i > x; i-- {
				if row[i] >= height {
					invisibleFromRight = true
					break
				}
			}

			if invisibleFromLeft && invisibleFromRight && invisibleFromBelow && invisibleFromAbove {
				invisibleTrees[treeCoordinate] = true
			}
		}

	}

	return mapLen*mapLen - len(invisibleTrees)
}

func getHighestScenicScore(treesMap [][]byte) int {
	var mapLen = len(treesMap)
	var maxScenicScore int

	for y, row := range treesMap {
		for x, height := range row {

			// outer trees
			if y == 0 || y == mapLen-1 || x == 0 || x == mapLen-1 {
				continue
			}

			currentTreeScenicScore := calculateScenicScore(y, x, height, treesMap)
			if currentTreeScenicScore > maxScenicScore {
				maxScenicScore = currentTreeScenicScore
			}

		}
	}

	return maxScenicScore
}

func calculateScenicScore(y, x int, height byte, treesMap [][]byte) int {
	var (
		upScore    int
		downScore  int
		leftScore  int
		rightScore int

		mapLen = len(treesMap)
	)

	for i := y - 1; i > -1; i-- {
		upScore += 1
		if treesMap[i][x] >= height {
			break
		}
	}

	for i := y + 1; i < mapLen; i++ {
		downScore += 1
		if treesMap[i][x] >= height {
			break
		}
	}

	for i := x + 1; i < mapLen; i++ {
		rightScore += 1
		if treesMap[y][i] >= height {
			break
		}
	}

	for i := x - 1; i > -1; i-- {
		leftScore += 1
		if treesMap[y][i] >= height {
			break
		}
	}

	return downScore * upScore * leftScore * rightScore
}
