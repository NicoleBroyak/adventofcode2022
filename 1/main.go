package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"strconv"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	var maxCalories int
	var currentElfCalories int
	for scanner.Scan() {
		text := scanner.Text()
		if text != "" {
			calories, _ := strconv.Atoi(text)
			currentElfCalories += calories
		} else {
			if currentElfCalories > maxCalories {
				maxCalories = currentElfCalories
			}
			currentElfCalories = 0
		}
	}

	dur := timer.Stop()
	log.Println(dur)
	log.Println(maxCalories)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	var maxCalories int
	var secondMaxCalories int
	var thirdMaxCalories int
	var currentElfCalories int
	for scanner.Scan() {
		text := scanner.Text()
		if text != "" {
			calories, _ := strconv.Atoi(text)
			currentElfCalories += calories
		} else {
			if currentElfCalories > maxCalories {
				thirdMaxCalories = secondMaxCalories
				secondMaxCalories = maxCalories
				maxCalories = currentElfCalories
			} else if currentElfCalories > secondMaxCalories {
				thirdMaxCalories = secondMaxCalories
				secondMaxCalories = currentElfCalories
			} else if currentElfCalories > thirdMaxCalories {
				thirdMaxCalories = currentElfCalories
			}
			currentElfCalories = 0
		}
	}

	dur := timer.Stop()
	log.Println(dur)
	log.Println(maxCalories + secondMaxCalories + thirdMaxCalories)
}
