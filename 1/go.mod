module gitlab.com/nicolebroyak/adventofcode2022/1

replace gitlab.com/nicolebroyak/adventofcode2022/utils v0.0.1 => ../utils

go 1.19

require gitlab.com/nicolebroyak/adventofcode2022/utils v0.0.1
