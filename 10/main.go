package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

type instruction struct {
	addx     bool
	strength int
}

func main() {
	testTask()
	testTask2()
	firstTask()
	secondExampleTask()
	secondTask()
}

func testTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	sum := sumCyclesStrength(instructions)
	log.Println(sum)

	dur := timer.Stop()
	log.Println(dur)
}

func testTask2() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input2.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	sum := sumCyclesStrength(instructions)
	log.Println(sum)

	dur := timer.Stop()
	log.Println(dur)
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	sum := sumCyclesStrength(instructions)
	log.Println(sum)

	dur := timer.Stop()
	log.Println(dur)
}

func secondExampleTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input2.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	msg := drawCRT(instructions)
	fmt.Println(msg)

	dur := timer.Stop()
	log.Println(dur)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	msg := drawCRT(instructions)
	fmt.Println(msg)

	dur := timer.Stop()
	log.Println(dur)
}

func parseInputIntoInstructions(scanner *bufio.Scanner) []instruction {
	var instructions []instruction
	for scanner.Scan() {
		row := strings.Split(scanner.Text(), " ")
		if len(row) == 2 {
			strength, _ := strconv.Atoi(row[1])
			instructions = append(instructions, instruction{addx: true, strength: strength})
		} else {
			instructions = append(instructions, instruction{addx: false, strength: 0})
		}
	}

	return instructions
}

func sumCyclesStrength(instructions []instruction) int {
	var sum int
	startStrength := 1
	var previousAddx int
	cycles := [242]int{}
	cycles[0] = startStrength
	var i = 1
	for _, instruction := range instructions {
		if !instruction.addx {
			cycles[i] = previousAddx + cycles[i-1]
			previousAddx = 0
			i++
		} else {
			cycles[i] = previousAddx + cycles[i-1]
			i++
			previousAddx = instruction.strength
			cycles[i] = previousAddx + cycles[i-1]
			previousAddx = 0
			i++

		}
	}
	cycles[i] = previousAddx + cycles[i-1]
	for i, strength := range cycles {
		if (i+21)%40 == 0 {
			sum += (i + 1) * strength
		}
	}
	return sum
}

func drawCRT(instructions []instruction) string {
	startStrength := 1
	var previousAddx int
	cycles := [242]int{}
	cycles[0] = startStrength
	crt := [242]bool{}
	crt[0] = cycles[0] >= -1 && cycles[0] <= 1
	var i = 1
	for _, instruction := range instructions {
		if !instruction.addx {
			cycles[i] = previousAddx + cycles[i-1]
			previousAddx = 0
			crt[i] = i%40-cycles[i] >= -1 && i%40-cycles[i] <= 1
			i++
		} else {
			cycles[i] = previousAddx + cycles[i-1]
			crt[i] = i%40-cycles[i] >= -1 && i%40-cycles[i] <= 1
			previousAddx = instruction.strength
			i++
			cycles[i] = previousAddx + cycles[i-1]
			crt[i] = i%40-cycles[i] >= -1 && i%40-cycles[i] <= 1
			previousAddx = 0
			i++

		}
	}
	cycles[i] = previousAddx + cycles[i-1]

	msg := ""
	for i, boolean := range crt {
		if i%40 == 0 && i != 0 {
			msg += "\n"
		}
		if boolean {
			msg += "#"
		} else {
			msg += "."
		}
	}

	return msg
}
