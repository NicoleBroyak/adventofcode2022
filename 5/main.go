package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

type stacksMap [][]byte
type instructionMaps []map[string]int

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	stacks, instructions := parseInputIntoStacksAndInstructions(scanner)
	stacks.executeInstructions(instructions, 9000)
	stacks.printTopItems()

	dur := timer.Stop()
	log.Println(dur)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	stacks, instructions := parseInputIntoStacksAndInstructions(scanner)
	stacks.executeInstructions(instructions, 9001)
	stacks.printTopItems()

	dur := timer.Stop()
	log.Println(dur)
}

func parseInputIntoStacksAndInstructions(scanner *bufio.Scanner) (stacksMap, instructionMaps) {
	var (
		rows        [][]byte
		rowsCount   int
		stacksCount int
	)

	instructions := make([]map[string]int, 0)
	for scanner.Scan() {
		text := scanner.Text()
		switch {

		// stacks row case structure: "[R]         [P] [G]     [J] [P] [T]"
		case strings.Contains(text, "["):
			var row []byte
			rowLen := len(text)
			for i := 0; i < rowLen; i++ {
				// each value is aligned at % 4 == 1 index, even it is whitespace
				if i%4 == 1 {
					row = append(row, text[i])
				}
			}
			rows = append(rows, row)
			rowsCount += 1

			// increase stacksCount if current row has more stacks than previous one
			if len(row) > stacksCount {
				stacksCount = len(row)
			}

		// instruction case structure: "move [quantity] from [source] to [destination]"
		case strings.Contains(text, "move"):
			splitText := strings.Split(text, " ")
			quantity, _ := strconv.Atoi(splitText[1])
			source, _ := strconv.Atoi(splitText[3])
			destination, _ := strconv.Atoi(splitText[5])
			instruction := map[string]int{
				"quantity":    quantity,
				"source":      source,
				"destination": destination,
			}
			instructions = append(instructions, instruction)
		default:
			continue
		}
	}

	stacks := make([][]byte, stacksCount)

	for i := len(rows) - 1; i >= 0; i-- {
		for stackNumOfItem := 0; stackNumOfItem < stacksCount; stackNumOfItem++ {
			if rows[i][stackNumOfItem] != 32 { // ignoring ASCII whitespace
				stacks[stackNumOfItem] = append(stacks[stackNumOfItem], rows[i][stackNumOfItem])
			}
		}
	}

	return stacksMap(stacks), instructionMaps(instructions)
}

func (s *stacksMap) executeInstructions(instructions instructionMaps, crateMoverVersion int) {
	stacksMapCurrent := *s
	sMap := [][]byte(stacksMapCurrent)
	for _, instruction := range instructions {
		quantity := instruction["quantity"]
		destination := instruction["destination"] - 1
		source := instruction["source"] - 1
		sourceLen := len(sMap[source])
		for i := 0; i < quantity; i++ {
			switch crateMoverVersion {
			case 9000:
				// LIFO-method
				sMap[destination] = append(sMap[destination], sMap[source][sourceLen-1-i])
			case 9001:
				// FIFO-like method
				sMap[destination] = append(sMap[destination], sMap[source][sourceLen-quantity+i])
			}
		}
		sMap[source] = sMap[source][:sourceLen-quantity]
	}

	stackMapNew := stacksMap(sMap)
	s = &stackMapNew
}

func (s stacksMap) printTopItems() {
	var msg string
	for _, stack := range s {
		msg += string(stack[len(stack)-1])
	}
	log.Println(msg)
}
