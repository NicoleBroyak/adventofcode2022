package main

import (
	"bufio"
	"bytes"
	"log"
	"os"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	var totalScore int
	for scanner.Scan() {
		text := scanner.Text()
		switch text {
		case "A Y":
			totalScore += 8
		case "B Y":
			totalScore += 5
		case "C Y":
			totalScore += 2
		case "A X":
			totalScore += 4
		case "B X":
			totalScore += 1
		case "C X":
			totalScore += 7
		case "A Z":
			totalScore += 3
		case "B Z":
			totalScore += 9
		case "C Z":
			totalScore += 6
		}
	}
	dur := timer.Stop()
	log.Println(dur)
	log.Println(totalScore)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	var totalScore int
	for scanner.Scan() {
		text := scanner.Text()
		switch text {
		case "A Y":
			totalScore += 4
		case "B Y":
			totalScore += 5
		case "C Y":
			totalScore += 6
		case "A X":
			totalScore += 3
		case "B X":
			totalScore += 1
		case "C X":
			totalScore += 2
		case "A Z":
			totalScore += 8
		case "B Z":
			totalScore += 9
		case "C Z":
			totalScore += 7
		}
	}
	dur := timer.Stop()
	log.Println(dur)
	log.Println(totalScore)
}
