package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	testTask()
	//firstTask()
}

func testTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	mapGrid, startPosition, endPosition := parseInputIntoMap(scanner)
	fmt.Println(mapGrid, startPosition, endPosition)
	steps := 0
	fewestSteps := 213721372137
	visitedPositions := make(map[[2]int]bool)
	reachToTheHighestPosition(mapGrid, startPosition, endPosition, &steps, &fewestSteps, visitedPositions)
	dur := timer.Stop()
	fmt.Println(fewestSteps)
	fmt.Println(dur)
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	mapGrid, startPosition, endPosition := parseInputIntoMap(scanner)
	fmt.Println(mapGrid, startPosition, endPosition)
	steps := 0
	fewestSteps := 213721372137
	visitedPositions := make(map[[2]int]bool)
	reachToTheHighestPosition(mapGrid, startPosition, endPosition, &steps, &fewestSteps, visitedPositions)
	dur := timer.Stop()
	fmt.Println(fewestSteps)
	fmt.Println(dur)
}

func parseInputIntoMap(scanner *bufio.Scanner) ([][]byte, [2]int, [2]int) {
	mapGrid := [][]byte{}
	startPosition := [2]int{}
	endPosition := [2]int{}
	for scanner.Scan() {
		mapGrid = append(mapGrid, scanner.Bytes())
	}
	fmt.Println(mapGrid)

	for y, row := range mapGrid {
		for x, val := range row {
			if val == 83 {
				mapGrid[y][x] = 97
				startPosition = [2]int{y, x}
			} else if val == 69 {
				endPosition = [2]int{y, x}
				mapGrid[y][x] = 122

			}
		}
	}
	return mapGrid, startPosition, endPosition
}

func reachToTheHighestPosition(mapGrid [][]byte, startPosition, endPosition [2]int, steps, fewestSteps *int, visitedPositions map[[2]int]bool) {
	currentPosition := startPosition
	visitedPositionsCopy := make(map[[2]int]bool)
	for k, v := range visitedPositions {
		visitedPositionsCopy[k] = v
	}
	visitedPositionsCopy[currentPosition] = true
	for true {
		fmt.Println("CURRENT", currentPosition)
		nextPositions := getPossibleNextPositions(currentPosition, mapGrid, visitedPositionsCopy)
		// fmt.Println("NEXT", nextPositions)
		for _, position := range nextPositions {
			if position[0] == endPosition[0] && position[1] == endPosition[1] {
				//		fmt.Println(*fewestSteps)
				if len(visitedPositionsCopy) < *fewestSteps {
					*fewestSteps = len(visitedPositionsCopy)
					fmt.Println(*fewestSteps)
				}
				break
			} else if visitedPositions[position] {
				continue
			} else {
				*steps++
				reachToTheHighestPosition(mapGrid, position, endPosition, steps, fewestSteps, visitedPositionsCopy)
			}
		}

		break
	}

	return
}

func getPossibleNextPositions(currentPosition [2]int, mapGrid [][]byte, visitedPositions map[[2]int]bool) [][2]int {
	possiblePositions := [][2]int{}
	consideredPositions := [][2]int{
		[2]int{currentPosition[0] + 1, currentPosition[1]},
		[2]int{currentPosition[0] - 1, currentPosition[1]},
		[2]int{currentPosition[0], currentPosition[1] + 1},
		[2]int{currentPosition[0], currentPosition[1] - 1},
	}

	for _, position := range consideredPositions {
		if visitedPositions[position] {
			continue
		}
		if position[0] == -1 || position[0] == len(mapGrid) || position[1] == -1 || position[1] == len(mapGrid[0]) {
			continue
		}
		switch mapGrid[position[0]][position[1]] - mapGrid[currentPosition[0]][currentPosition[1]] {
		case 0, 1:
			possiblePositions = append(possiblePositions, position)
		default:
			continue
		}
	}
	return possiblePositions
}
