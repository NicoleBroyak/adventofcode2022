package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)
	var fullyContainingPairs int

	for scanner.Scan() {
		line := strings.Split(scanner.Text(), ",")
		firstElf := strings.Split(line[0], "-")
		secondElf := strings.Split(line[1], "-")
		firstElfMin, _ := strconv.Atoi(firstElf[0])
		firstElfMax, _ := strconv.Atoi(firstElf[1])
		secondElfMin, _ := strconv.Atoi(secondElf[0])
		secondElfMax, _ := strconv.Atoi(secondElf[1])
		if (firstElfMin >= secondElfMin && firstElfMax <= secondElfMax) ||
			(secondElfMin >= firstElfMin && secondElfMax <= firstElfMax) {
			fullyContainingPairs += 1
		}

	}

	dur := timer.Stop()
	log.Println(dur)
	log.Println(fullyContainingPairs)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)
	var overlappingPairs int

	for scanner.Scan() {
		line := strings.Split(scanner.Text(), ",")
		firstElf := strings.Split(line[0], "-")
		secondElf := strings.Split(line[1], "-")
		firstElfMin, _ := strconv.Atoi(firstElf[0])
		firstElfMax, _ := strconv.Atoi(firstElf[1])
		secondElfMin, _ := strconv.Atoi(secondElf[0])
		secondElfMax, _ := strconv.Atoi(secondElf[1])
		if (firstElfMin >= secondElfMin && firstElfMax <= secondElfMax) ||
			(secondElfMin >= firstElfMin && secondElfMax <= firstElfMax) ||
			(firstElfMin < secondElfMin && firstElfMax >= secondElfMin) ||
			(secondElfMin < firstElfMin && secondElfMax >= firstElfMin) {
			overlappingPairs += 1
		}

	}

	dur := timer.Stop()
	log.Println(dur)
	log.Println(overlappingPairs)
}

// 20-30
// 21-29
// 19-30
// 19-31
// 20-30
// 20-31
// 19-29
// 29-39
