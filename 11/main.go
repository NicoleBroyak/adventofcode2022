package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	testTask()
	firstTask()
	secondTestTask()
	secondTask()
}

func testTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	monkeys := parseInputIntoGameStartState(scanner)
	for i := 0; i < 20; i++ {
		monkeys.throwItems(3)
	}
	monkeyBusiness := monkeys.getMonkeyBusiness()
	log.Println(monkeyBusiness)
	dur := timer.Stop()
	log.Println(dur)
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	monkeys := parseInputIntoGameStartState(scanner)
	for i := 0; i < 20; i++ {
		monkeys.throwItems(3)
	}
	monkeyBusiness := monkeys.getMonkeyBusiness()
	log.Println(monkeyBusiness)
	dur := timer.Stop()
	log.Println(dur)
}

func secondTestTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	monkeys := parseInputIntoGameStartState(scanner)
	for i := 0; i < 10000; i++ {
		monkeys.throwItemsBigInts()
	}
	monkeyBusiness := monkeys.getMonkeyBusiness()
	log.Println(monkeyBusiness)
	dur := timer.Stop()
	log.Println(dur)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	monkeys := parseInputIntoGameStartState(scanner)
	for i := 0; i < 10000; i++ {
		monkeys.throwItemsBigInts()
	}
	monkeyBusiness := monkeys.getMonkeyBusiness()
	log.Println(monkeyBusiness)
	dur := timer.Stop()
	log.Println(dur)
}

type monkey struct {
	possesedItemsWorryLevels    []uint64
	inspectedItems              int
	operationType               string
	operationFactor             uint64
	operationFactorAsOld        bool
	divisibleByTest             uint64
	monkeyToThrowIfDivisible    int
	monkeyToThrowIfNotDivisible int
}

type monkeys []monkey

func (m monkeys) getMonkeyBusiness() int {
	var max int
	var secondMax int
	for _, monkey := range m {
		if monkey.inspectedItems > max {
			secondMax = max
			max = monkey.inspectedItems
		} else if monkey.inspectedItems > secondMax {
			secondMax = monkey.inspectedItems
		}
	}
	return max * secondMax
}

func (m *monkeys) throwItems(worryDivider uint64) {
	monkeys := *m
	for i, monkey := range monkeys {
		for _, item := range monkey.possesedItemsWorryLevels {
			if monkey.operationFactorAsOld {
				if (item*item/worryDivider)%monkey.divisibleByTest == 0 {
					monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels,
						(item*item)/worryDivider,
					)
				} else {
					monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels,
						(item*item)/worryDivider,
					)
				}
			} else if monkey.operationType == "*" {
				if (item*monkey.operationFactor/worryDivider)%monkey.divisibleByTest == 0 {
					monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels,
						(item*monkey.operationFactor)/worryDivider,
					)
				} else {
					monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels,
						(item*monkey.operationFactor)/worryDivider,
					)
				}
			} else if monkey.operationType == "+" {
				if ((item+monkey.operationFactor)/worryDivider)%monkey.divisibleByTest == 0 {
					monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels,
						(item+monkey.operationFactor)/worryDivider,
					)
				} else {
					monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels,
						(item+monkey.operationFactor)/worryDivider,
					)
				}
			}
			monkeys[i].inspectedItems++
			monkeys[i].possesedItemsWorryLevels = monkeys[i].possesedItemsWorryLevels[1:]
		}
	}

	m = &monkeys
}

func (m *monkeys) throwItemsBigInts() {
	monkeys := *m
	var worryDividerLCM = uint64(1)
	for _, monkey := range monkeys {
		worryDividerLCM *= monkey.divisibleByTest
	}
	for i, monkey := range monkeys {
		for _, item := range monkey.possesedItemsWorryLevels {
			if monkey.operationFactorAsOld {
				item = item * item
				item = item % worryDividerLCM
				if item%monkey.divisibleByTest == 0 {
					monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels,
						item,
					)
				} else {
					monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels,
						item,
					)
				}
			} else if monkey.operationType == "*" {
				item = item * monkey.operationFactor
				item = item % worryDividerLCM
				if item%monkey.divisibleByTest == 0 {
					monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels,
						item,
					)
				} else {
					monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels,
						item,
					)
				}
			} else if monkey.operationType == "+" {
				item = item + monkey.operationFactor
				item = item % worryDividerLCM
				if item%monkey.divisibleByTest == 0 {
					monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfDivisible].possesedItemsWorryLevels,
						item,
					)
				} else {
					monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels = append(
						monkeys[monkey.monkeyToThrowIfNotDivisible].possesedItemsWorryLevels,
						item,
					)
				}

			}
			monkeys[i].inspectedItems++
			monkeys[i].possesedItemsWorryLevels = monkeys[i].possesedItemsWorryLevels[1:]
		}
	}

	m = &monkeys
}

func parseInputIntoGameStartState(scanner *bufio.Scanner) monkeys {
	monkeys := monkeys{}
	var monkeyNum = -1
	for scanner.Scan() {
		row := scanner.Text()
		switch {
		case strings.Contains(row, "Monkey"):
			monkeys = append(monkeys, monkey{})
			monkeyNum++
		case strings.Contains(row, "Starting items"):
			row = strings.Trim(row, "Starting items:")
			items := strings.Split(row, ", ")
			for _, item := range items {
				itemNum, _ := strconv.Atoi(item)
				monkeys[monkeyNum].possesedItemsWorryLevels = append(monkeys[monkeyNum].possesedItemsWorryLevels, uint64(itemNum))
			}
		case strings.Contains(row, "Operation"):
			if strings.Contains(row, "*") {
				monkeys[monkeyNum].operationType = "*"
			} else {
				monkeys[monkeyNum].operationType = "+"
			}
			index := strings.LastIndex(row, " ") + 1
			operationFactor := row[index:len(row)]

			if operationFactor == "old" {
				monkeys[monkeyNum].operationFactorAsOld = true
			} else {
				operationFactorNum, _ := strconv.Atoi(operationFactor)
				monkeys[monkeyNum].operationFactor = uint64(operationFactorNum)

			}

		case strings.Contains(row, "Test"):
			index := strings.LastIndex(row, " ") + 1
			divisibleByTest, _ := strconv.Atoi(row[index:len(row)])
			monkeys[monkeyNum].divisibleByTest = uint64(divisibleByTest)

		case strings.Contains(row, "If true"):
			index := strings.LastIndex(row, " ") + 1
			monkeys[monkeyNum].monkeyToThrowIfDivisible, _ = strconv.Atoi(row[index:len(row)])
		case strings.Contains(row, "If false"):
			index := strings.LastIndex(row, " ") + 1
			monkeys[monkeyNum].monkeyToThrowIfNotDivisible, _ = strconv.Atoi(row[index:len(row)])
		}
	}

	return monkeys
}
