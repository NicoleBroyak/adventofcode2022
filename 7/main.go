package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)
	pathsSize := parseInput(scanner)
	sum := sumPathsBelowSize(pathsSize, 100000)
	log.Println(sum)
	dur := timer.Stop()
	log.Println(dur)
}

func secondTask() {

	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)
	pathsSize := parseInput(scanner)
	const diskSpace = 70000000
	const neededSpace = 30000000
	usedSpace := pathsSize["/"]
	freeSpace := diskSpace - usedSpace
	neededExtraSpace := neededSpace - freeSpace
	dirSize := findDirectoryToDelete(pathsSize, neededExtraSpace)
	log.Println(dirSize)
	dur := timer.Stop()
	log.Println(dur)
}

func parseInput(scanner *bufio.Scanner) map[string]int {
	var currentPath []string

	pathsSize := make(map[string]int)

	for scanner.Scan() {
		text := scanner.Text()
		splitText := strings.Split(text, " ")
		size, err := strconv.Atoi(splitText[0])
		switch {
		case strings.Contains(text, "$ cd"):
			if strings.Contains(text, "$ cd ..") {
				currentPath = currentPath[:len(currentPath)-1]
			} else if strings.Contains(text, "$ cd /") {
				currentPath = []string{"/"}
			} else {
				currentPath = append(currentPath, splitText[2])
			}
		case strings.Contains(text, "$ ls"):
			continue
		case text[0:2] == "dir":
			log.Println("contains dir")
		case err == nil:
			var pathName string
			for _, path := range currentPath {
				pathName += path
				pathsSize[pathName] += size
			}
		}
	}

	return pathsSize
}

func sumPathsBelowSize(pathsSize map[string]int, maxSize int) int {
	var sum int
	for _, size := range pathsSize {
		if size <= maxSize {
			sum += size
		}
	}
	return sum
}

func findDirectoryToDelete(pathsSize map[string]int, neededSpace int) int {
	sizeOfDeletedDir := pathsSize["/"] // max value for start
	for path, size := range pathsSize {
		if path == "/" || size < neededSpace {
			continue
		}
		if size < sizeOfDeletedDir {
			sizeOfDeletedDir = size
		}
	}
	return sizeOfDeletedDir
}
