package main

import (
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testCase struct {
	code              string
	lengthOfRope      int
	ropeState         rope
	instructions      []instruction
	expectedRopeState rope
}

func TestBasic(t *testing.T) {
	headPoint := coordinates{x: 2, y: 2}
	testCases := []testCase{
		testCase{
			code:         "11U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "11R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "11D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 1, y: 1},
			},
		},
		testCase{
			code:         "11L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 1, y: 1},
			},
		},
		testCase{
			code:         "12U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 1, y: 2},
			},
		},
		testCase{
			code:         "12R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "12D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 1, y: 2},
			},
		},
		testCase{
			code:         "12L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 1, y: 2},
			},
		},

		testCase{
			code:         "13U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 1, y: 3},
			},
		},
		testCase{
			// 3-2 vs 1-3 ----> (2)-(-1) ----> 3-2 vs 2-2
			code:         "13R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "13D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "13L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 1, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 1, y: 3},
			},
		},
		testCase{
			code:         "21U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "21R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 1},
			},
		},
		testCase{
			code:         "21D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 2, y: 1},
			},
		},
		testCase{
			code:         "21L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 2, y: 1},
			},
		},
		testCase{
			code:         "22U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "22R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "22D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "22L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "23U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 2, y: 3},
			},
		},
		testCase{
			code:         "23R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 3},
			},
		},
		testCase{
			code:         "23D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "23L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 2, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 2, y: 3},
			},
		},
		testCase{
			code:         "31U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "31R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 3, y: 1},
			},
		},
		testCase{
			code:         "31D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 3, y: 1},
			},
		},
		testCase{
			code:         "31L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 1},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 2, y: 2},
			},
		},

		testCase{
			code:         "32U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 3, y: 2},
			},
		},
		testCase{
			code:         "32R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 3, y: 2},
			},
		},
		testCase{
			code:         "32D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 3, y: 2},
			},
		},
		testCase{
			code:         "32L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 2},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 2, y: 2},
			},
		},

		testCase{
			code:         "33U1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 3},
				coordinates{x: 3, y: 3},
			},
		},
		testCase{
			code:         "33R1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 3, y: 2},
				coordinates{x: 3, y: 3},
			},
		},
		testCase{
			code:         "33D1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 2, y: 1},
				coordinates{x: 2, y: 2},
			},
		},
		testCase{
			code:         "33L1",
			lengthOfRope: 2,
			ropeState: rope{
				headPoint,
				coordinates{x: 3, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 2},
				coordinates{x: 2, y: 2},
			},
		},
	}

	for _, testCase := range testCases {
		testCase.ropeState.executeInstructions(testCase.instructions)
		if !assert.Equal(t, testCase.expectedRopeState, testCase.ropeState) {
			log.Println(testCase.code)
		}
	}
}

func TestAdvanced(t *testing.T) {
	testCases := []testCase{
		testCase{
			code: "U4",
			ropeState: rope{
				coordinates{x: 4, y: 0},
				coordinates{x: 3, y: 0},
				coordinates{x: 2, y: 0},
				coordinates{x: 1, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    4,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 4, y: 4},
				coordinates{x: 4, y: 3},
				coordinates{x: 4, y: 2},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
		},
		testCase{
			code: "L3",
			ropeState: rope{
				coordinates{x: 4, y: 4},
				coordinates{x: 4, y: 3},
				coordinates{x: 4, y: 2},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    3,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 4},
				coordinates{x: 2, y: 4},
				coordinates{x: 3, y: 3},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
		},
		testCase{
			code: "D1",
			ropeState: rope{
				coordinates{x: 1, y: 4},
				coordinates{x: 2, y: 4},
				coordinates{x: 3, y: 3},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    1,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 1, y: 3},
				coordinates{x: 2, y: 4},
				coordinates{x: 3, y: 3},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
		},
		testCase{
			code: "R4",
			ropeState: rope{
				coordinates{x: 1, y: 3},
				coordinates{x: 2, y: 4},
				coordinates{x: 3, y: 3},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    4,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 5, y: 3},
				coordinates{x: 4, y: 3},
				coordinates{x: 3, y: 3},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
		},
		testCase{
			code: "L5",
			ropeState: rope{
				coordinates{x: 5, y: 2},
				coordinates{x: 4, y: 3},
				coordinates{x: 3, y: 3},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    5,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 0, y: 2},
				coordinates{x: 1, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 3, y: 2},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
		},
	}

	for _, testCase := range testCases {
		testCase.ropeState.executeInstructions(testCase.instructions)
		if !assert.Equal(t, testCase.expectedRopeState, testCase.ropeState) {
			log.Println(testCase.code)
		}
	}
}

func TestAdvancedExample(t *testing.T) {
	testCases := []testCase{
		testCase{
			code: "R5",
			ropeState: rope{
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    5,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 5, y: 0},
				coordinates{x: 4, y: 0},
				coordinates{x: 3, y: 0},
				coordinates{x: 2, y: 0},
				coordinates{x: 1, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
		},
		testCase{
			code: "U8",
			ropeState: rope{
				coordinates{x: 5, y: 0},
				coordinates{x: 4, y: 0},
				coordinates{x: 3, y: 0},
				coordinates{x: 2, y: 0},
				coordinates{x: 1, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "U",
					motion:    8,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 5, y: 8},
				coordinates{x: 5, y: 7},
				coordinates{x: 5, y: 6},
				coordinates{x: 5, y: 5},
				coordinates{x: 5, y: 4},
				coordinates{x: 4, y: 4},
				coordinates{x: 3, y: 3},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
			},
		},
		testCase{
			code: "L8",
			ropeState: rope{
				coordinates{x: 5, y: 8},
				coordinates{x: 5, y: 7},
				coordinates{x: 5, y: 6},
				coordinates{x: 5, y: 5},
				coordinates{x: 5, y: 4},
				coordinates{x: 4, y: 4},
				coordinates{x: 3, y: 3},
				coordinates{x: 2, y: 2},
				coordinates{x: 1, y: 1},
				coordinates{x: 0, y: 0},
			},
			instructions: []instruction{
				instruction{
					direction: "L",
					motion:    8,
				},
			},
			expectedRopeState: rope{
				coordinates{x: -3, y: 8},
				coordinates{x: -2, y: 8},
				coordinates{x: -1, y: 8},
				coordinates{x: 0, y: 8},
				coordinates{x: 1, y: 8},
				coordinates{x: 1, y: 7},
				coordinates{x: 1, y: 6},
				coordinates{x: 1, y: 5},
				coordinates{x: 1, y: 4},
				coordinates{x: 1, y: 3},
			},
		},
		testCase{
			code: "D3",
			ropeState: rope{
				coordinates{x: -3, y: 8},
				coordinates{x: -2, y: 8},
				coordinates{x: -1, y: 8},
				coordinates{x: 0, y: 8},
				coordinates{x: 1, y: 8},
				coordinates{x: 1, y: 7},
				coordinates{x: 1, y: 6},
				coordinates{x: 1, y: 5},
				coordinates{x: 1, y: 4},
				coordinates{x: 1, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    3,
				},
			},
			expectedRopeState: rope{
				coordinates{x: -3, y: 5},
				coordinates{x: -3, y: 6},
				coordinates{x: -2, y: 7},
				coordinates{x: -1, y: 7},
				coordinates{x: 0, y: 7},
				coordinates{x: 1, y: 7},
				coordinates{x: 1, y: 6},
				coordinates{x: 1, y: 5},
				coordinates{x: 1, y: 4},
				coordinates{x: 1, y: 3},
			},
		},
		testCase{
			code: "R17",
			ropeState: rope{
				coordinates{x: -3, y: 5},
				coordinates{x: -3, y: 6},
				coordinates{x: -2, y: 7},
				coordinates{x: -1, y: 7},
				coordinates{x: 0, y: 7},
				coordinates{x: 1, y: 7},
				coordinates{x: 1, y: 6},
				coordinates{x: 1, y: 5},
				coordinates{x: 1, y: 4},
				coordinates{x: 1, y: 3},
			},
			instructions: []instruction{
				instruction{
					direction: "R",
					motion:    17,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 14, y: 5},
				coordinates{x: 13, y: 5},
				coordinates{x: 12, y: 5},
				coordinates{x: 11, y: 5},
				coordinates{x: 10, y: 5},
				coordinates{x: 9, y: 5},
				coordinates{x: 8, y: 5},
				coordinates{x: 7, y: 5},
				coordinates{x: 6, y: 5},
				coordinates{x: 5, y: 5},
			},
		},
		testCase{
			code: "D10",
			ropeState: rope{
				coordinates{x: 14, y: 5},
				coordinates{x: 13, y: 5},
				coordinates{x: 12, y: 5},
				coordinates{x: 11, y: 5},
				coordinates{x: 10, y: 5},
				coordinates{x: 9, y: 5},
				coordinates{x: 8, y: 5},
				coordinates{x: 7, y: 5},
				coordinates{x: 6, y: 5},
				coordinates{x: 5, y: 5},
			},
			instructions: []instruction{
				instruction{
					direction: "D",
					motion:    10,
				},
			},
			expectedRopeState: rope{
				coordinates{x: 14, y: -5},
				coordinates{x: 14, y: -4},
				coordinates{x: 14, y: -3},
				coordinates{x: 14, y: -2},
				coordinates{x: 14, y: -1},
				coordinates{x: 14, y: 0},
				coordinates{x: 13, y: 0},
				coordinates{x: 12, y: 0},
				coordinates{x: 11, y: 0},
				coordinates{x: 10, y: 0},
			},
		},
	}

	for _, testCase := range testCases {
		testCase.ropeState.executeInstructions(testCase.instructions)
		if !assert.Equal(t, testCase.expectedRopeState, testCase.ropeState) {
			log.Println(testCase.code)
		}
	}
}
