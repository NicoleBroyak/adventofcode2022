package main

import (
	"bufio"
	"bytes"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

type instruction struct {
	direction string
	motion    int
}

func main() {
	testTask()
	firstTask()
	secondExampleTask()
	secondTask()
}

func testTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	rope := newRope(2)
	count := rope.executeInstructions(instructions)
	log.Println(count)

	dur := timer.Stop()
	log.Println(dur)
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	rope := newRope(2)
	count := rope.executeInstructions(instructions)
	log.Println(count)

	dur := timer.Stop()
	log.Println(dur)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	rope := newRope(10)
	count := rope.executeInstructions(instructions)
	log.Println(count)

	dur := timer.Stop()
	log.Println(dur)
}

func secondExampleTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./test_input2.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)

	instructions := parseInputIntoInstructions(scanner)
	rope := newRope(10)
	count := rope.executeInstructions(instructions)
	log.Println(count)

	dur := timer.Stop()
	log.Println(dur)
}

func parseInputIntoInstructions(scanner *bufio.Scanner) []instruction {
	var instructions []instruction
	for scanner.Scan() {
		row := strings.Split(scanner.Text(), " ")
		motion, _ := strconv.Atoi(row[1])
		instructions = append(instructions, instruction{direction: row[0], motion: motion})
	}

	return instructions
}

type rope []coordinates

func newRope(lengthOfRope int) rope {
	return make([]coordinates, lengthOfRope)
}

func (r *rope) executeInstructions(instructions []instruction) int {
	rope := *r
	tailPositions := make(map[string]bool)
	for _, instruction := range instructions {
		for count := 0; count < instruction.motion; count++ {
			for i, _ := range rope {
				if i == 0 {
					moveHead(&rope[0], instruction.direction)
				} else {
					movePoint(&rope[i], rope[i-1])
					if i == len(*r)-1 {
						tailPositions[rope[i].String()] = true
					}
				}
			}
		}
	}

	r = &rope

	return len(tailPositions)

}

func moveHead(head *coordinates, direction string) {
	switch direction {
	case "U":
		head.y++
	case "R":
		head.x++
	case "D":
		head.y--
	case "L":
		head.x--
	}
}

func movePoint(point *coordinates, pointAhead coordinates) {
	yDiff := pointAhead.y - point.y
	xDiff := pointAhead.x - point.x
	diffCode := fmt.Sprintf("(%d)-(%d)", xDiff, yDiff)
	switch diffCode {
	case "(-2)-(0)":
		// 1-3 vs 3-3 ---> 1-3 vs 2-3
		point.x--
	case "(0)-(-2)":
		// 3-1 vs 3-3 ---> 3-1 vs 3-2
		point.y--
	case "(2)-(0)":
		// 5-3 vs 3-3 ---> 5-3 vs 4-3
		point.x++
	case "(0)-(2)":
		// 3-5 vs 3-3 ---> 3-5 vs 3-4
		point.y++

	case "(-2)-(-1)", "(-1)-(-2)", "(-2)-(-2)":
		// 1-2 vs 3-3 ----> 1-2 vs 2-2
		// 2-1 vs 3-3 ----> 2-1 vs 2-2
		// 1-1 vs 3-3 ----> 1-1 vs 2-2
		point.x--
		point.y--
	case "(2)-(1)", "(1)-(2)", "(2)-(2)":
		// 5-4 vs 3-3 ----> 5-4 vs 4-4
		// 4-5 vs 3-3 ----> 4-5 vs 4-4
		// 5-5 vs 3-3 ----> 5-5 vs 4-4
		point.x++
		point.y++
	case "(-2)-(1)", "(-1)-(2)", "(-2)-(2)":
		// 1-4 vs 3-3 ----> 1-4 vs 2-4
		// 2-5 vs 3-3 ----> 2-5 vs 2-4
		// 1-5 vs 3-3 ----> 1-5 vs 2-4
		point.x--
		point.y++

	case "(1)-(-2)", "(2)-(-1)", "(2)-(-2)":
		// 4-1 vs 3-3 ----> 4-1 vs 4-2
		// 5-2 vs 3-3 ----> 5-2 vs 4-2
		// 5-1 vs 3-3 ----> 5-1 vs 4-2
		point.x++
		point.y--
	default:
		return
	}

}

type coordinates struct {
	x int
	y int
}

func (c coordinates) String() string {
	return fmt.Sprintf("(%d)-(%d)", c.x, c.y)
}
