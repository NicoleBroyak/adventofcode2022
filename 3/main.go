package main

import (
	"bufio"
	"bytes"
	"log"
	"os"
	"strings"

	"gitlab.com/nicolebroyak/adventofcode2022/utils"
)

func main() {
	firstTask()
	secondTask()
}

func firstTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)
	var prioritySum int
	for scanner.Scan() {
		var matchedItems string
		line := scanner.Text()
		firstCompartment := line[:len(line)/2]
		secondCompartment := line[len(line)/2:]
		for _, item := range []byte(firstCompartment) {
			if strings.Contains(secondCompartment, string(item)) && !strings.Contains(matchedItems, string(item)) {
				matchedItems = matchedItems + string(item)
			}
		}
		prioritySum += convertStringIntoPrioritySum(matchedItems)

	}

	dur := timer.Stop()
	log.Println(dur)
	log.Println(prioritySum)
}

func secondTask() {
	timer := utils.StartTimer()

	file, err := os.ReadFile("./input.txt")
	if err != nil {
		return
	}

	reader := bytes.NewReader(file)
	scanner := bufio.NewScanner(reader)
	var prioritySum int
	var i int
	var firstLine string
	var secondLine string
	var thirdLine string
	for scanner.Scan() {
		switch i % 3 {
		case 0:
			if i != 0 {
				matchedItems := findMatchingItems(firstLine, secondLine, thirdLine)
				prioritySum += convertStringIntoPrioritySum(matchedItems)
			}
			firstLine = scanner.Text()
		case 1:
			secondLine = scanner.Text()
		case 2:
			thirdLine = scanner.Text()
		}

		i++
	}

	matchedItems := findMatchingItems(firstLine, secondLine, thirdLine)
	prioritySum += convertStringIntoPrioritySum(matchedItems)

	dur := timer.Stop()
	log.Println(dur)
	log.Println(prioritySum)
}

func convertStringIntoPrioritySum(matchedItems string) int {
	var prioritySum int
	for _, item := range []byte(matchedItems) {
		if int(item) > 96 {
			prioritySum += int(item) - 96
		} else {
			prioritySum += int(item) - 38
		}
	}

	return prioritySum
}

func findMatchingItems(firstLine, secondLine, thirdLine string) string {
	var matchedItems string
	for _, item := range []byte(firstLine) {
		if strings.Contains(secondLine, string(item)) && strings.Contains(thirdLine, string(item)) {
			if !strings.Contains(matchedItems, string(item)) {
				matchedItems += string(item)
			}
		}
	}

	return matchedItems
}
